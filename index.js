const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 8080;

const dbConfig = require('./config/database');

// routers
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');
const userRouter = require('./routers/userRouter');

// database connection
// mongoose.connect(`mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.databaseName}`, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//     useFindAndModify: false,
//     useCreateIndex: true
// });
mongoose.connect(`mongodb+srv://${dbConfig.username}:${dbConfig.password}@cluster0.qrzyc.mongodb.net/${dbConfig.databaseName}?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

app.use(cors());
app.use(express.json());

app.use('/api', authRouter);
app.use('/api', noteRouter);
app.use('/api', userRouter);

app.listen(port, () => {
    console.log(`Server is listening on ${port} port.`);
});