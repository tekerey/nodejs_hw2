const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/authMiddleware');
const { getUser,
        changePassword,
        deleteUser } = require('../controllers/userController');

router.get('/users/me', authMiddleware, getUser);
router.delete('/users/me', authMiddleware, deleteUser);
router.patch('/users/me', authMiddleware, changePassword);

module.exports = router;