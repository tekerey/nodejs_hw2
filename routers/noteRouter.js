const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/authMiddleware');
const { getNotes, createNote,
    getNoteById, updateNoteById,
    changeNoteStatus, deleteNote } = require('../controllers/noteController');

router.get('/notes', authMiddleware, getNotes);
router.post('/notes', authMiddleware, createNote);
router.get('/notes/:id', authMiddleware, getNoteById);
router.put('/notes/:id', authMiddleware, updateNoteById);
router.patch('/notes/:id', authMiddleware, changeNoteStatus);
router.delete('/notes/:id', authMiddleware, deleteNote);

module.exports = router;