const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('note', new Schema({
    userId: {
        type: String,
        required: true,
    },
    completed: {
        type: Boolean,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    createdDate: {
        type: String,
        required: true
    }
}));