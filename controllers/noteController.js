const Note = require('../models/note');

module.exports.getNotes = (req, res) => {
    const userId = req.userInfo._id;
    
    Note.find({userId}).select({__v: false}).exec()
        .then(notes => {
            res.status(200).json({notes});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
}

module.exports.createNote = (req, res) => {
    const userId = req.userInfo._id;
    const { text } = req.body;

    if (!text) {
        return res.status(400).json({message: 'No text provided.'});
    }

    const note = new Note({
        userId,
        completed: false,
        text,
        createdDate: new Date().toISOString()
    });

    note.save()
        .then(() => {
            res.status(200).json({message: 'Success'});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
}

module.exports.getNoteById = (req, res) => {
    const { id } = req.params;
    const userId = req.userInfo._id;

    if (!id) {
        return res.status(400).json({message: 'No id provided.'});
    }

    Note.findOne({_id: id, userId}).select({__v: false}).exec()
        .then(note => {
            if (!note) {
                return res.status(400).json({message: 'No note with that id.'});
            }
            res.status(200).json({note});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
}

module.exports.updateNoteById = (req, res) => {
    const userId = req.userInfo._id;
    const { id } = req.params;
    const { text } = req.body;

    if (!id) {
        return res.status(400).json({message: 'No id provided.'});
    }
    if (!text) {
        return res.status(400).json({message: 'No text provided.'});
    }

    Note.findOneAndUpdate({_id: id, userId}, {text}).exec()
        .then(note => {
            if (!note) {
                return res.status(400).json({message: 'No note with that id.'});
            }
            res.status(200).json({message: 'Success'});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
}

module.exports.changeNoteStatus = (req, res) => {
    const userId = req.userInfo._id;
    const { id } = req.params;

    if (!id) {
        return res.status(400).json({message: 'No id provided.'});
    }

    Note.findOne({_id: id, userId}).exec()
        .then(note => {
            if (!note) {
                return res.status(400).json({message: 'No note with that id.'});
            }
            Note.updateOne({_id: note._id}, {completed: !note.completed})
                .then(() => {
                    res.status(200).json({message: 'Success'});
                }).catch(err => {
                    res.status(500).json({message: err.message});
                });
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
}

module.exports.deleteNote = (req, res) => {
    const userId = req.userInfo._id;
    const { id } = req.params;

    if (!id) {
        return res.status(400).json({message: 'No id provided.'});
    }

    Note.findOneAndDelete({_id: id, userId}).exec()
        .then(note => {
            if (!note) {
                return res.status(400).json({message: 'No note with that id.'});
            }
            res.status(200).json({message: 'Success'});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
}