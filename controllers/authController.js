const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const User = require('../models/user');

const { secret } = require('../config/auth');

module.exports.register = (req, res) => {
    const { username, password } = req.body;
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({
            message: 'Required credentials are not provided'
        });
    }

    User.findOne({username}).exec()
        .then(user => {
            if (user) {
                return res.status(400).json({message: 'User already exist.'});
            }
            user = new User({
                username,
                password: crypto.createHash('sha256').update(password).digest('hex'),
                createdDate: new Date().toISOString()
            });
            user.save()
                .then(() => {
                    res.status(200).json({
                        message: 'Success'
                    });
                })
                .catch(err => {
                    res.status(500).json({
                        message: err.message
                    });
                });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
}

module.exports.login = (req, res) => {
    const { username, password } = req.body;
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({
            message: 'Required credentials are not provided'
        });
    }

    User.findOne({username}).exec()
        .then(user => {
            if (!user) {
                return res.status(400).json({message: 'User not found'});
            }
            if (user.password != crypto.createHash('sha256').update(password).digest('hex')) {
                return res.status(400).json({message: 'Wrong password'});
            }
            res.status(200).json({
                jwt_token: jwt.sign({
                    _id: user._id,
                    username: user.username,
                    createdDate: user.createdDate
                }, secret, {expiresIn: '1h'})
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
}