const User = require('../models/user');
const crypto = require('crypto');

module.exports.getUser = (req, res) => {
    const { _id } = req.userInfo;

    User.findById(_id).select({password: false, __v: false}).exec()
        .then(user => {
            if (!user) {
                return res.status(400).json({message: 'No user with that id.'});
            }
            res.status(200).json({user});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
}

module.exports.changePassword = (req, res) => {
    const { _id } = req.userInfo;
    const { oldPassword, newPassword } = req.body;

    if (!oldPassword) {
        return res.status(400).json({message: 'Old password is not provided.'});
    }
    if (!newPassword) {
        return res.status(400).json({message: 'New password is not provided.'});
    }

    User.findById(_id).exec()
        .then(user => {
            if (!user) {
                return res.status(400).json({message: 'No user with that id.'});
            }
            // should check old password before changing it
            const hash = crypto.createHash('sha256')
                .update(oldPassword)
                .digest('hex');
            if (user.password != hash) {
                return res.status(400).json({message: 'Wrong old password'});
            }
            // Change password
            user.password = crypto.createHash('sha256')
                .update(newPassword).digest('hex');
            user.save()
                .then(() => {
                    res.status(200).json({message: 'Success'});
                })
                .catch(err => {
                    res.status(500).json({message: err.message});
                });
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
}

module.exports.deleteUser = (req, res) => {
    const { _id } = req.userInfo;

    User.findByIdAndDelete(_id).exec()
        .then(user => {
            if (!user) {
                return res.status(400).json({message: 'No user with that id.'});
            }
            res.status(200).json({message: 'Success'});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
    // should we delete all user's notes?
}