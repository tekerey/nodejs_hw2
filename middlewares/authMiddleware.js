const { secret } = require('../config/auth');
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    const authHeader = req.headers['authorization'];

    if (!authHeader) {
        return res.status(401).json({message: 'No authorization header.'});
    }

    // Should support both?
    // Authorization: {token}
    // Authorization: Bearer {token}
    const parts = authHeader.split(' ');
    const token = parts[1] || parts[0];
    try {
        req.userInfo = jwt.verify(token, secret);
        next();
    } catch (err) {
        return res.status(401).json({message: 'Invalid JWT'});
    }
}